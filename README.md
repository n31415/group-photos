# Group Photos

## What it does

When you give the application a list of photos, it will search for a qr code in each photo.
We call each photo were one is found a "separator".
The photos between one separator and the next are all put in one group and then renamed.

The new name is configurable, but it should probably always contain the group name (which is the data from the qr code).

The order in which the photos are considered is important for the grouping.
Have a look at the help to see which options you have.

## Installation

### zbar

The package pyzbar used to decode the qr codes needs the zbar shared library.
For windows, the DLLs are shipped with zbar, on other operating systems, you need to install it manually.
The provided script `install_zbar.sh` should work for a lot of Linuxes and MacOS.

### Via pipx

You can install the latest script directly from the gitlab package repo via

```
pipx install group-photos --index-url https://gitlab.com/api/v4/projects/54253659/packages/pypi/simple
```

After that you should be able to run `group-photos`.

Theoretically, this also works for pip, but this is strongly discouraged and deactivated by default in newer versions of pip. See [PEP 668](https://peps.python.org/pep-0668/).

### Via poetry

Run `poetry install`. After that you should be able to run `poetry group-photos`.

## Help

```
usage: group-photos [-h] [--processes PROCESSES] [--include-sep] [--rename-format RENAME_FORMAT]
                    [--target-dir TARGET_DIR] [--reverse]
                    [--as-is | --alphabetical | --last-changed | --recorded]
                    [--warn | --throw | --ignore] [--move | --copy | --print]
                    pictures [pictures ...]

positional arguments:
  pictures              Pictures to process.

options:
  -h, --help            show this help message and exit
  --processes PROCESSES, -p PROCESSES
                        How many processes to usefor detecting qr codes. Will calculate a reasonable
                        default if unset. Set to 1 for sequential processing.
  --include-sep         If set, also rename the "separator" image. Otherwise, it is untouched.
  --rename-format RENAME_FORMAT, -f RENAME_FORMAT
                        Format string for the new name of the file. Can contain the following variables:
                        GROUP:   Content of the QR code of the last picture that contains one.
                        INDEX:   Index of the image in its group.
                        SUFFIX:  Suffix (e.g. ".jpg") of the orignal image.
                        STEM:    Stem of the orignal image (part before suffix).
                        See https://docs.python.org/3/library/string.html#formatstrings for format
                        string specification.
  --target-dir TARGET_DIR, -t TARGET_DIR
                        Directory where to put the renamed pictures. If unset uses the directory where
                        an image came from. Relative directories are resolved relative to the working
                        directory.
  --reverse, -r         Reverse the order of paths.

Ordering:
  Order in which the picture groups are determined.

  --as-is               Use order as provided on command line.
  --alphabetical        Alphabetical by file name.
  --last-changed        Chronologically (oldest first) by FILE change time.
  --recorded            Chronologically (oldest first) by phote recording time. (Default)

None handling:
  How to handle images that couldn't be grouped.

  --warn                Warn about them. (Default)
  --throw               Throw an exception.
  --ignore              Ignore them.

Rename method:
  In which way the renaming is done.

  --move                Move the files. (Default)
  --copy                Copy the files. This might use a lot of storage.
  --print               Just print the source and target (shell escaped, one source and target per
                        line).
```
