import datetime
import os
import pathlib
import sys
from collections.abc import Iterable
from enum import Enum
from typing import TypeAlias, TypeVar, cast

from PIL import ExifTags, Image

Key = TypeVar("Key")
Group: TypeAlias = dict[Key, list[pathlib.Path]]


class Ordering(Enum):
    AsIs = "as_is"
    LastChanged = "last_changed"
    Alphabetical = "alphabetical"
    Recorded = "recorded"


class NoneHandling(Enum):
    Ignore = "ignore"
    Throw = "throw"
    Warn = "warn"


def group(
    qr_mapping: dict[pathlib.Path, str | None],
    order: Ordering,
    include_sep: bool,
    reverse: bool,
) -> Group[str | None]:
    sorted_paths = sort_paths(qr_mapping, order, reverse)

    last = None
    result: Group[None | str] = {None: []}

    for p in sorted_paths:
        data = qr_mapping[p]
        if data is not None:
            last = data
            result[last] = [p] if include_sep else []
        else:
            result[last].append(p)

    return result


def handle_none_values(
    mapping: Group[str | None], none_handling: NoneHandling
) -> Group[str]:
    result = cast(Group[str], mapping)
    if None not in mapping:
        return result

    none_values = mapping.pop(None)
    if not none_values:
        return result

    warn_message = "The following images couldn't be grouped to a group:" + ", ".join(
        map(str, none_values)
    )

    if none_handling == NoneHandling.Throw:
        raise ValueError(warn_message)
    if none_handling == NoneHandling.Warn:
        print(warn_message, file=sys.stderr)

    return result


def sort_paths(
    paths: Iterable[pathlib.Path], order: Ordering, reverse: bool
) -> list[pathlib.Path]:
    if order == Ordering.LastChanged:
        return sorted(paths, key=lambda p: p.stat().st_mtime, reverse=reverse)
    if order == Ordering.Alphabetical:
        return sorted(paths, key=lambda p: p.name, reverse=reverse)
    if order == Ordering.Recorded:
        return sorted(paths, key=get_recording_timestamp, reverse=reverse)

    if reverse:
        return list(paths)[::-1]
    return list(paths)


def get_recording_timestamp(path: pathlib.Path) -> str:
    with Image.open(path) as image:
        exif = image.getexif()
    try:
        return cast(str, exif[ExifTags.Base.DateTime])
    except KeyError:
        # No exif timestamp, fallback to ctime
        epoch = os.path.getmtime(path)
        dt = datetime.datetime.fromtimestamp(epoch)
        return dt.strftime("%Y:%m:%d %H:%M:%S")
