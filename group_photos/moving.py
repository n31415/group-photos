import pathlib
import shlex
import shutil
from enum import Enum


class MoveMethod(Enum):
    Move = "move"
    Copy = "copy"
    Print = "print"


def move(
    grouping: dict[str, list[pathlib.Path]],
    rename_format: str,
    method: MoveMethod,
    target_dir: pathlib.Path | None,
) -> None:
    def do_move(source: pathlib.Path, target: str) -> None:
        if target_dir is None:
            target_path = source.with_name(target)
        else:
            target_path = target_dir / target
        if method == MoveMethod.Copy:
            shutil.copy2(source, target_path)
        elif method == MoveMethod.Move:
            source.rename(target_path)
        else:
            print(shlex.join((str(source), str(target_path))))

    for key, group in grouping.items():
        for index, path in enumerate(group):
            target = rename_format.format(
                STEM=path.stem, GROUP=key.strip(), INDEX=index, SUFFIX=path.suffix
            ).replace("/", "_")

            do_move(path, target)
