import argparse
from enum import Enum
from typing import Any, Generic, TypeVar

T = TypeVar("T", bound=Enum)


class EnumGroup(Generic[T]):
    def __init__(
        self, parser: argparse.ArgumentParser, title: str, description: str, dest: str
    ):
        self.parser = parser
        self.group = self.parser.add_argument_group(
            title, description
        ).add_mutually_exclusive_group()
        self.dest = dest
        self.added_values: set[T] = set()
        self.default: T | None = None

    def set_default(self, default: T) -> None:
        assert self.default is None, "There can only be one default value."
        self.parser.set_defaults(**{self.dest: default})
        self.default = default

    def add_argument(self, enum: T, help: str, default: bool = False) -> None:
        self.added_values.add(enum)
        if default:
            self.set_default(enum)
            help = help.rstrip() + " (Default)"
        arg = enum.value.lower().replace("_", "-")
        self.group.add_argument(
            f"--{arg}",
            dest=self.dest,
            const=enum,
            help=help,
            action="store_const",
        )

    def __enter__(self) -> "EnumGroup[T]":
        return self

    def __exit__(self, *_: Any) -> None:
        assert len(self.added_values) > 0, "Need to add some values to group."
        enum_type = type(next(iter(self.added_values)))
        assert (
            set(enum_type) == self.added_values
        ), f"All values of {enum_type} are required. \nAdded: {self.added_values}\nNeeded: {set(enum_type)}"
        assert self.default is not None, "The default value needs to be set."
