import pathlib
from os import cpu_count
from typing import cast

import pyzbar.pyzbar as zbar
from PIL import Image
from tqdm import tqdm
from tqdm.contrib.concurrent import process_map


def detect_all(
    paths: list[pathlib.Path], processes: int | None = None
) -> dict[pathlib.Path, str | None]:
    if processes == 1:
        return dict(tqdm(map(detect_assoc, paths), total=len(paths)))

    workers = guess_workers(processes)

    return dict(
        process_map(  # type: ignore # It is untyped and I can do nothing about that
            detect_assoc,
            paths,
            chunksize=guess_chunksize(len(paths), workers),
            max_workers=workers,
            total=len(paths),
        )
    )


def detect(path: pathlib.Path) -> str | None:
    with Image.open(path) as image:
        # Downsize image to max 1024x1024 because zbar cannot recognize QR code in very large images
        image.thumbnail((1024, 1024))
        result = zbar.decode(image, symbols=[zbar.ZBarSymbol.QRCODE])

    if len(result) > 1:
        raise ValueError("More than one qr code found...")

    if not result:
        return None

    return cast(bytes, result[0].data).decode()


def detect_assoc(path: pathlib.Path) -> tuple[pathlib.Path, str | None]:
    return path, detect(path)


def guess_workers(workers: int | None) -> int:
    if workers is None:
        cpus = cpu_count() or 0
        return min(32, cpus + 4)

    return workers


def guess_chunksize(length: int, pool_length: int) -> int:
    chunksize, extra = divmod(length, pool_length * 4)
    if extra:
        chunksize += 1

    return chunksize
