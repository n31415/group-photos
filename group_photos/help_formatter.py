import argparse


class KeepNewlineFormatter(argparse.HelpFormatter):
    def _split_lines(self, text: str, width: int) -> list[str]:
        lines = text.splitlines()
        import textwrap

        return sum(
            (textwrap.wrap(line, width) for line in lines),
            start=[],
        )
