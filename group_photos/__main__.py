import argparse
import pathlib

from group_photos.detection import detect_all
from group_photos.enum_group import EnumGroup
from group_photos.grouping import NoneHandling, Ordering, group, handle_none_values
from group_photos.help_formatter import KeepNewlineFormatter
from group_photos.moving import MoveMethod, move


def arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=KeepNewlineFormatter)

    parser.add_argument(
        "--processes",
        "-p",
        type=int,
        default=None,
        help="How many processes to usefor detecting qr codes. "
        "Will calculate a reasonable default if unset. "
        "Set to 1 for sequential processing.",
    )
    parser.add_argument(
        "pictures", nargs="+", help="Pictures to process.", type=pathlib.Path
    )
    parser.add_argument(
        "--include-sep",
        action="store_true",
        help='If set, also rename the "separator" image. Otherwise, it is untouched.',
    )
    parser.add_argument(
        "--rename-format",
        "-f",
        default="{GROUP}_{INDEX}{SUFFIX}",
        help="Format string for the new name of the file. Can contain the following variables:\n"
        "GROUP:\t Content of the QR code of the last picture that contains one.\n"
        "INDEX:\t Index of the image in its group.\n"
        'SUFFIX:\t Suffix (e.g. ".jpg") of the orignal image.\n'
        "STEM:\t Stem of the orignal image (part before suffix).\n"
        "See https://docs.python.org/3/library/string.html#formatstrings for format string specification.",
    )
    parser.add_argument(
        "--target-dir",
        "-t",
        default=None,
        help="Directory where to put the renamed pictures. "
        "If unset uses the directory where an image came from. "
        "Relative directories are resolved relative to the working directory.",
    )
    parser.add_argument(
        "--reverse", "-r", action="store_true", help="Reverse the order of paths."
    )

    with EnumGroup[Ordering](
        parser,
        title="Ordering",
        description="Order in which the picture groups are determined.",
        dest="ordering",
    ) as ordering_group:
        ordering_group.add_argument(
            Ordering.AsIs, "Use order as provided on command line."
        )
        ordering_group.add_argument(Ordering.Alphabetical, "Alphabetical by file name.")
        ordering_group.add_argument(
            Ordering.LastChanged,
            "Chronologically (oldest first) by FILE change time.",
        )
        ordering_group.add_argument(
            Ordering.Recorded,
            "Chronologically (oldest first) by phote recording time.",
            default=True,
        )

    with EnumGroup[NoneHandling](
        parser,
        title="None handling",
        description="How to handle images that couldn't be grouped.",
        dest="none_handling",
    ) as none_handling_group:
        none_handling_group.add_argument(
            NoneHandling.Warn, "Warn about them.", default=True
        )
        none_handling_group.add_argument(NoneHandling.Throw, "Throw an exception.")
        none_handling_group.add_argument(NoneHandling.Ignore, "Ignore them.")

    with EnumGroup[MoveMethod](
        parser,
        title="Rename method",
        description="In which way the renaming is done.",
        dest="move",
    ) as none_handling_group:
        none_handling_group.add_argument(
            MoveMethod.Move, "Move the files.", default=True
        )
        none_handling_group.add_argument(
            MoveMethod.Copy, "Copy the files. This might use a lot of storage."
        )
        none_handling_group.add_argument(
            MoveMethod.Print,
            "Just print the source and target (shell escaped, one source and target per line).",
        )

    return parser


def main() -> None:
    args = arg_parser().parse_args()

    detected = detect_all(args.pictures, processes=args.processes)
    groups = group(detected, args.ordering, args.include_sep, args.reverse)
    none_removed = handle_none_values(groups, args.none_handling)
    move(none_removed, args.rename_format, args.move, args.target_dir)


if __name__ == "__main__":
    main()
