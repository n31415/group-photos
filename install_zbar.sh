#!/bin/sh

if command -v brew; then
    echo "Installing with brew..."
    brew install zbar
elif command -v pacman; then
    echo "Installing with pacman..."
    sudo pacman -S zbar
elif command -v apt-get; then
    echo "Installing with apt-get..."
    sudo apt-get install libzbar0
else
    echo "Could not find brew, pacman and apt-get, please install zbar library yourself."
fi
